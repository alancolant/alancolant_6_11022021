## Piquante

Le projet a été généré avec [Angular CLI](https://github.com/angular/angular-cli) version 7.0.2.

Pour faire fonctionner le projet, vous devez installer node-sass à part.

## Development FRONTEND

Dans le dossier `P6_99_frontend`, démarrer `ng serve` pour avoir accès au serveur de développement. Rendez-vous
sur `http://localhost:4200/`. L'application va se recharger automatiquement si vous modifiez un fichier source.

## Development BACKEND

Dans le dossier `P6_01_backend`, démarrer `node server` pour avoir accès à l'API sur le port 3000

## Dependences

Pour installer les dependances, executer ``npm install`` ou ``yarn`` dans le dossier  `P6_01_backend`
ou  `P6_99_frontend`
