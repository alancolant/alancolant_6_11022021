module.exports = (request, res, next) => {
    let headers = {};

    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Content-Type, Content-Length, Authorization, Accept, X-Requested-With"
    );
    res.header(
        "Access-Control-Allow-Methods",
        "GET, POST, PATCH, DELETE, OPTIONS, PUT"
    );
    if (request.method === 'OPTIONS') {
        res.sendStatus(200);
    } else {
        next();
    }

};