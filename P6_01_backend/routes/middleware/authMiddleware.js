const User = require("../../app/Models/User");
const jwt = require('jsonwebtoken');
const config = require('../../config/app')();

module.exports = (request, res, next) => {
    try {
        const token = request.header('Authorization').replace('Bearer ', '');
        const decodedToken = jwt.verify(token, config.randomKey);
        const userId = decodedToken.userId;

        //On vérifie que l'id contenu dans le corps de la requête soit le même que celui de l'authentification
        if (request.fields.userId && request.fields.userId !== userId) {
            return res.status(401).json({error: 'Invalid request!'})
        }

        User.findOne({_id: userId}).exec((err, authUser) => {
            //On retourne si le token ne nous permet pas de définir un utilisateur
            if (authUser == null) {
                return res.status(401).json({error: 'Authentification problem!'})
            }
            //On implémente l'utilisateur dans le requête
            request.authUser = authUser;
            next();
        });
    } catch (err) {
        return res.status(401).json({error: 'Authentification problem!'})
    }
};