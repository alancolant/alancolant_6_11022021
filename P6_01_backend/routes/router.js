const AuthController = require("../app/Controllers/AuthController");
const SauceController = require("../app/Controllers/SauceController");

const express = require('express');
const router = express.Router();
const cors = require('./middleware/corsMiddleware')
const auth = require('./middleware/authMiddleware')
const formidable = require('express-formidable');
const throttle = require("express-throttle"); //Protection contre spam

//Routes relatives aux middleware(cors et parsage des requetes)
router.use(cors)
router.use(formidable());

//Routes relatives à l'authentification
router.post('/auth/login', throttle({"rate": "1/s"}), AuthController.login)
router.post('/auth/signup', throttle({"rate": "5/s"}), AuthController.signup)

//Routes relatives aux sauces (protégée par Bearer Token)
router.get('/sauces', auth, SauceController.list)
router.get('/sauces/:id', auth, SauceController.view)
router.put('/sauces/:id', throttle({"rate": "2/s"}), auth, SauceController.update)
router.post('/sauces', auth, throttle({"rate": "1/s"}), SauceController.store)
router.delete('/sauces/:id', throttle({"rate": "1/s"}), auth, SauceController.delete)
router.post('/sauces/:id/like', auth, SauceController.like_unlike)

module.exports = router;