const express = require('express')
const app = express()
const config = require('./config/app')();
const database = require('./app/Database.js');

//On initialise la connection à la DB
database()
    .then(() => {
        //Initialisation du routeur sur /api
        const router = require('./routes/router');

        //Route d'accès aux photos
        app.use('/images/', express.static(config.imagePath));

        //Routes API
        app.use('/api/', router);

        //On démarre le serveur
        app.listen(config.port, () => {
            console.log(`BackEnd démarré sur http://localhost:${config.port}`)
        })
    })
    .catch((err) => {
        //On affiche l'erreur si on n'arrive pas à se connecter à MongoDB
        console.error('Impossible de se connecter.');
        console.error(err);
    });