const app = {
    local: {
        port: 3000,
        database: "localdb",
        imagePath: "storage/images",
        randomKey: "MUkyx+6B97LAIXTw1IedEQ=="
    },
    production: {
        port: 3000,
        database: "atlas",
        imagePath: "storage/images_prod",
        randomKey: "TAnd6j0GTbJAJiz0qcoZng=="
    },
    current: 'production'
}

module.exports = function (key) {
    //Si pas de clé défini on récupère le mode par défaut
    if (key === undefined) {
        return app[app.current];
    } else {
        return app[key];
    }
};