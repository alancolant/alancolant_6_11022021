const mongoose = require("mongoose");
const bcrypt = require('bcrypt');
const config = require('../../config/app')();
const jwt = require('jsonwebtoken');
const jumblator = require("mongoose-jumblator").fieldEncryptionPlugin;

const Schema = new mongoose.Schema({
    email: {
        type: String,
        encrypt: true,
        searchable: true,
        required: true,
    },
    password: {
        type: String,
        required: true
    }
})
//Ajout du plugin uniqueValidator
Schema.plugin(jumblator, {secret: "47@#ererss587"});

//Hash automatique du mot de passe
Schema.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, 10);
    }
    return next();
});

//Methode pour vérifier un mot de passe
Schema.methods.checkPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

//Methode pour générer un token
Schema.methods.generateToken = function () {
    return jwt.sign(
        {userId: this._id},
        config.randomKey,
        {expiresIn: '24h'}
    )
};

module.exports = mongoose.model('User', Schema);