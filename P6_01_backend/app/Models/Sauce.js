const mongoose = require("mongoose");
const Schema = new mongoose.Schema({

    //Relation vers owner
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },

    name: {
        type: String,
        required: true,
    },
    manufacturer: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    mainPepper: {
        type: String,
        required: true
    },
    imageUrl: {
        type: String,
        default: null
    },
    heat: {
        type: Number,
        default: 0,
        max: 10,
        min: 0,
    },
    likes: {
        type: Number,
        default: 0,
    },
    dislikes: {
        type: Number,
        default: 0
    },

    //Relation vers userLiked
    usersLiked: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],

    //Relation vers userDisliked
    usersDisliked: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
})

//Mise à jour automatique du compteur de like et de dislike
Schema.pre('save', function (next) {
    if (this.isModified('usersLiked')) {
        this.likes = this.usersLiked.length;
    }
    if (this.isModified('usersDisliked')) {
        this.dislikes = this.usersDisliked.length;
    }
    return next();
});
module.exports = mongoose.model('Sauce', Schema);