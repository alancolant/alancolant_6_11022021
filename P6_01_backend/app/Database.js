const mongoose = require('mongoose');

const config = require('../config/app')();
const dbConfig = require('../config/database');

module.exports = function () {
    // On configure mongoDb pour utiliser des promesse
    mongoose.Promise = global.Promise;
    mongoose.set('useFindAndModify', false);

    //Récupération des informations de connexions
    const db = dbConfig[config.database];

    // On initialise la connection selon l'environnement
    let cnx = 'mongodb://' + db.host + ':' + db.port + '/' + db.database;
    if (db.user && db.password) {
        cnx = 'mongodb+srv://' + db.user + ':' + db.password + '@' + db.host + '/' + db.database
    }
    return mongoose.connect(cnx, {
        useCreateIndex: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });
};