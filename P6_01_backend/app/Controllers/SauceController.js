const Sauce = require("../Models/Sauce");
const mongoose = require("mongoose");
const fs = require("fs");
const config = require('../../config/app')();

//Protégé par un middleware Bearer TOKEN
class SauceController {
    //Retourne la liste de toutes les sauces
    list(req, res, next) {
        Sauce.find({}).exec(function (err, sauces) {
            if (err) {
                return next(err);
            }
            res.json(sauces);
        });
    }

    //Enregistre une nouvelle sauce
    store(req, res, next) {

        //On parse la chaine de caractère reçue
        req.fields.sauce = JSON.parse(req.fields.sauce)
        //Génération d'un id aléatoire
        req.fields.sauce._id = mongoose.Types.ObjectId();

        //On récupère l'image
        let image = req.files.image;

        //On récupère l'extension de l'image et on génère un nouveau nom de fichier
        const extension = image.name.split('.').pop()
        const newName = req.fields.sauce._id + '.' + extension;

        const newPath = __dirname + '/../../' + config.imagePath + '/' + newName;

        //On déplace le fichier temporaire
        fs.rename(image.path, newPath, function (err) {
            if (err) {
                return next(err);
            }
            //On génère l'URL de l'image
            req.fields.sauce.imageUrl = req.protocol + '://' + req.get('host') + '/images/' + newName
            //On enregistre la sauce
            const sauce = new Sauce(req.fields.sauce);
            sauce.save(err => {
                if (err) {
                    return next(err)
                }
                //Envoi de la réponse
                res.status(201).json({message: "Sauce crée!"});
            });
        });
    }

    //Retourne une sauce en particulier
    view(req, res, next) {
        //Récupération de l'id
        const id = req.params.id;
        Sauce.findById(id).exec(function (err, sauces) {
            if (err) {
                return res.status(404).json({err});
            }
            res.json(sauces);
        });
    }

    //Supprime une sauce en particulier
    delete(req, res, next) {
        const id = req.params.id;
        Sauce.findOneAndDelete({_id: id}, function (err, sauce) {
            if (err) {
                return next(err);
            }
            //On supprime l'image associée
            const filename = sauce.imageUrl.split('/').pop();
            const filepath = __dirname + '/../../' + config.imagePath + '/' + filename;
            fs.unlink(filepath, (err) => {
                if (err && err.code !== 'ENOENT') {
                    return next(err);
                }
                Sauce.findById(id).exec(function (err, sauces) {
                    //On ne renvoie que l'erreur que si ce n'est pas une erreur d'absence du fichier
                    if (err) {
                        return next(err);
                    }
                    res.json({message: "Sauce supprimée!"});
                });
            });
        });
    }

    //Ajoute ou retire un like selon l'utilisateur
    like_unlike(req, res, next) {
        const id = req.params.id;

        Sauce.findById(id).exec(function (err, sauce) {
            if (err) {
                return next(err);
            }
            //Si c'est un dislike
            if (req.fields.like === -1) {
                //On supprime du tableau des liked users
                const indexLiked = sauce.usersLiked.indexOf(req.fields.userId);
                if (indexLiked > -1) {
                    sauce.usersLiked.splice(indexLiked, 1);
                }

                //On ajoute au tableau des disliked users si il n'est pas déjà dedans
                const indexDisliked = sauce.usersDisliked.indexOf(req.fields.userId);
                if (indexDisliked === -1) {
                    sauce.usersDisliked.push(req.fields.userId);
                }
            }
            //Si c'est un like
            else if (req.fields.like === 1) {
                //On supprime du tableau des disliked users
                const indexDisliked = sauce.usersDisliked.indexOf(req.fields.userId);
                if (indexDisliked > -1) {
                    sauce.usersDisliked.splice(indexDisliked, 1);
                }

                //On ajoute au tableau des disliked users si il n'est pas déjà dedans
                const indexLiked = sauce.usersLiked.indexOf(req.fields.userId);
                if (indexLiked === -1) {
                    sauce.usersLiked.push(req.fields.userId);
                }
            }
            //Si c'est ni un like ni un dislike
            else if (req.fields.like === 0) {
                //On supprime des liked et disliked users
                const indexDisliked = sauce.usersDisliked.indexOf(req.fields.userId);
                if (indexDisliked > -1) {
                    sauce.usersDisliked.splice(indexDisliked, 1);
                }

                const indexLiked = sauce.usersLiked.indexOf(req.fields.userId);
                if (indexLiked > -1) {
                    sauce.usersLiked.splice(indexLiked, 1);
                }
            }

            //On enregistre la nouveauté
            sauce.save(err => {
                if (err) {
                    return next(err)
                }
                //Envoi de la réponse
                res.json({"message": "Avis modifié!"});
            });
        });
    }

    //Modifie une sauce
    update(req, res, next) {
        const id = req.params.id;

        //On regarde la structure de la requête pour savoir s'il y'a une image
        let hasPhoto = false;
        let newSauce = {};
        if (req.fields.sauce !== undefined) {
            newSauce = JSON.parse(req.fields.sauce)
            hasPhoto = true;
        } else {
            newSauce = req.fields;
        }

        //On met à jour l'entrée avec les nouvelles valeurs
        Sauce.findByIdAndUpdate(id, newSauce, null, (err, sauce) => {
            if (err) {
                return next(err);
            }

            //On met à jour l'image si elle existe
            if (hasPhoto) {
                //Suppression de l'ancienne photo
                const filename = sauce.imageUrl.split('/').pop();
                const filepath = __dirname + '/../../' + config.imagePath + '/' + filename;
                fs.unlink(filepath, (err) => {
                    if (err) {
                        return res.status(500).json({error: err});
                    }
                    //On déplace la photo de tmp
                    //On récupère l'image
                    let image = req.files.image;

                    //On récupère l'extension de l'image et on génère un nouveau nom de fichier
                    const extension = image.name.split('.').pop()
                    const newName = sauce._id + '.' + extension;

                    const newPath = __dirname + '/../../' + config.imagePath + '/' + newName;

                    //On déplace le fichier temporaire
                    fs.rename(image.path, newPath, function (err) {
                        if (err) {
                            return res.status(500).json({error: err});
                        }
                        //On génère l'URL de l'image
                        sauce.imageUrl = req.protocol + '://' + req.get('host') + '/images/' + newName
                        //On enregistre la sauce
                        sauce.save(err => {
                            if (err) {
                                return res.status(400).json({error: err});
                            }
                            //Envoi de la réponse
                            return res.json({message: "Sauce mise à jour!"});
                        });
                    });
                });

            } else {
                return res.json({message: "Sauce mise à jour!"});
            }
        });

    }
}


module.exports = new SauceController();