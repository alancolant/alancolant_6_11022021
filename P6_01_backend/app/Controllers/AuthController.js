const User = require("../Models/User");
const sanitize = require('mongo-sanitize');

class AuthController {
    login(req, res, next) {
        User.findOne({email: sanitize(req.fields.email)}).exec((err, user) => {
                if (err) {
                    return next(err);
                }
                if (user == null) {
                    return res.status(401).json({error: 'Adresse email introuvable!'});
                }

                if (user.checkPassword(req.fields.password)) {
                    return res.json({"userId": user._id, "token": user.generateToken()});
                } else {
                    return res.status(401).json({error: 'Invalid request!'})
                }
            }
        );
    }

    signup(req, res, next) {
        //Récupération pour unicité
        User.findOne({email: sanitize(req.fields.email)}).exec(
            (err, user) => {
                if (user !== null) {
                    return res.status(400).json({error: 'Email déjà utilisé!'})
                } else {
                    const user = new User({
                        email: sanitize(req.fields.email),
                        password: req.fields.password
                    });
                    user.save(err => {
                        if (err) {
                            //En cas d'erreur (email déjà utilisé)
                            return res.status(400).json({error: err})
                        } else {
                            return res.status(201).json({message: "Utilisateur crée"})
                        }
                    });
                }
            });
    }
}


module.exports = new AuthController();